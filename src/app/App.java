package app;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Flyweight Demo");
        for(int i=0; i <= 10000; ++i) {
            Circle circle = (Circle)FlyweightFactory.getCircle(getRandomColor());
            
            circle.setX(getRandomX());
            circle.setY(getRandomY());
            circle.setRadius(100);
            circle.draw();
            System.out.println("Total circle draws: " + i);
        }
        
		Runtime rt = Runtime.getRuntime();
		long total_mem = rt.totalMemory();
		long free_mem = rt.freeMemory();
		long used_mem = total_mem - free_mem;	
		System.out.println("Amount of total memory: " + total_mem/1024/1024 +"MB");
		System.out.println("Amount of free memory: " + free_mem/1024/1024 + "MB");
		System.out.println("Amount of used memory: " + used_mem/1024/1024 +"MB");
		
    }
    private static final String colors[] = { "Red", "Green", "Blue", "White", "Black" };
    
    private static String getRandomColor() {
        return colors[(int)(Math.random()*colors.length)];
    }
    
     private static int getRandomX() {
        return (int)(Math.random()*100 );
    }
    private static int getRandomY() {
       return (int)(Math.random()*100);
    }
    
}