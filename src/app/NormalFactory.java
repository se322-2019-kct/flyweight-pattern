package app;

import java.util.HashMap;

public class NormalFactory {

    public static Shape getCircle(String color) {
        Circle circle = new Circle(color);
        System.out.println("Creating circle of color : " + color);
        return circle;
    }
}
